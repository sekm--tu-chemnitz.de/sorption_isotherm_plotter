import pandas as pd
import os
import matplotlib.pyplot as plt
import src.tuc_colors as tuc_colors
import src.convert as convert


plot_single_figure = False
plot_subfigure = True

output_name = '6all.eps'

height = 15   # cm
width = 15

output_path = f'output/{output_name}'
font_size = 10
marker_size = 10
x_label = r'$p\quad/\quad \mathrm{MPa}$'
y_label = r'$m_\mathrm{sorp}\quad/\quad \mathrm{mmol\cdot g_{sam}^{-1}}$'


def plot_sub(colors, markers, labels, folder_name, pos_x, pos_y, marker_size):
    sc_plots = []
    if not len(dir_container[folder_name]):
        axs[pos_x, pos_y].axis('off')
    else:
        for key, color, label, m in zip(dir_container[folder_name], colors, labels, markers):
            x_values = dir_container[folder_name][key]['p/MPa']
            y_values = dir_container[folder_name][key]['Msorp / mmol/g_sam']
            if label != '':
                sc = axs[pos_x, pos_y].scatter(x_values, y_values, label=label, s=marker_size, marker=m,
                                               facecolor=color, edgecolor='face')
                sc_plots.append(sc)
            else:
                sc = axs[pos_x, pos_y].scatter(x_values, y_values, s=marker_size, marker=m,
                                               facecolor=color, edgecolor='face')
        axs[pos_x, pos_y].set_title(f'({folder_name})', loc='right')
        return sc_plots


if plot_single_figure:
    input_path = 'input/single_figure/'

    x_label = r'$p\quad/\quad \mathrm{MPa}$'
    y_label = r'$m_\mathrm{sorp}\quad/\quad \mathrm{mmol\cdot g_{sam}^{-1}}$'

    file_container = {}
    for file in sorted(os.listdir(input_path), reverse=False):
        if file.endswith('.txt'):
            df = pd.read_csv(input_path+file, decimal=',', delimiter='\t')
            file_container.update({file: df})

    colors = [tuc_colors.darkgreen, tuc_colors.green, tuc_colors.darkgreen, tuc_colors.darkgreen, tuc_colors.darkred,
              tuc_colors.darkred]
    markers = ['o', 's', 'o', '^', 'o', '^']

    plt.rcParams.update({'font.size': font_size})
    fig, ax = plt.subplots(figsize=convert.cm2inch(width, height))
    for key, color, m in zip(file_container, colors, markers):
        ax.scatter(file_container[key]['p/MPa'], file_container[key]['Msorp / mmol/g_sam'], s=10, marker=m, facecolor=color,
                   edgecolor='face', label=rf'$\rm{key[:-4]}$')

    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    ax.tick_params(axis='x', direction='in')   # alternatives: direction='in' / 'out' / 'inout'
    ax.tick_params(axis='y', direction='in')

    #plt.legend(loc='best', frameon=False)
    plt.tight_layout()

elif plot_subfigure:
    sc_labels = []
    sc_handles = []
    input_path = 'input/subfigure/'
    dir_container = {}
    for directory in sorted(os.listdir(input_path)):
        file_container = {}
        for file in os.listdir(f'{input_path}/{directory}'):
            if file.endswith('.txt'):
                df = pd.read_csv(f'{input_path}/{directory}/{file}', decimal=',', delimiter='\t')
                file_container.update({file: df})
        dir_container.update({directory: file_container})

    plt.rcParams.update({'font.size': font_size})
    fig, axs = plt.subplots(nrows=3, ncols=2, figsize=convert.cm2inch(width, height), sharex=True)

    ### Figure a
    colors = [tuc_colors.darkblue, tuc_colors.blue]
    markers = ['o', 's']
    labels_a = ['raw 1', 'raw 2']
    for l_a in labels_a:
        sc_labels.append(l_a)
    sc_a = plot_sub(colors, markers, labels_a, folder_name='a', pos_x=0, pos_y=0, marker_size=marker_size)
    for sc in sc_a:
        sc_handles.append(sc)

    ### Figure b
    colors = [tuc_colors.darkgreen, tuc_colors.green]
    markers = ['o', 's']
    labels_b = ['ph97 1', 'ph97 2']
    for l_b in labels_b:
        sc_labels.append(l_b)
    sc_b = plot_sub(colors, markers, labels_b, folder_name='b', pos_x=0, pos_y=1, marker_size=marker_size)
    for sc in sc_b:
        sc_handles.append(sc)

    ### Figure c
    colors = [tuc_colors.darkblue, tuc_colors.darkblue]
    markers = ['^', 'o']
    labels_c = ['raw silyl', '']
    for l_c in labels_c:
        if l_c != '':
            sc_labels.append(l_c)
    sc_c = plot_sub(colors, markers, labels_c, folder_name='c', pos_x=1, pos_y=0, marker_size=marker_size)
    for sc in sc_c:
        sc_handles.append(sc)

    ### Figure d
    colors = [tuc_colors.darkred, tuc_colors.darkred]
    markers = ['^', 'o']
    labels_d = ['H2O silyl', 'H2O']
    for l_d in labels_d:
        sc_labels.append(l_d)
    sc_d = plot_sub(colors, markers, labels_d, folder_name='d', pos_x=1, pos_y=1, marker_size=marker_size)
    for sc in sc_d:
        sc_handles.append(sc)

    ### Figure e
    colors = [tuc_colors.darkgreen, tuc_colors.darkgreen]
    markers = ['^', 'o']
    labels_e = ['ph97 silyl', '']
    for l_e in labels_e:
        sc_labels.append(l_e)
    sc_e = plot_sub(colors, markers, labels_e, folder_name='e', pos_x=2, pos_y=0, marker_size=marker_size)
    for sc in sc_e:
        sc_handles.append(sc)

    ### Figure f
    colors = [tuc_colors.darkred, tuc_colors.darkred, tuc_colors.darkgreen, tuc_colors.darkgreen, tuc_colors.darkblue,
              tuc_colors.darkblue]
    markers = ['^', 'o', '^', 'o', '^', 'o']
    labels_f = ['', '', '', '', '', '']
    for l_f in labels_f:
        sc_labels.append(l_f)
    sc_f = plot_sub(colors, markers, labels_f, folder_name='f', pos_x=2, pos_y=1, marker_size=marker_size)
    for sc in sc_f:
        sc_handles.append(sc)

    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.tick_params(axis='x', direction='in')  # alternatives: direction='in' / 'out' / 'inout'
        ax.tick_params(axis='y', direction='in')
        ax.xaxis.set_ticks_position('both')
        ax.yaxis.set_ticks_position('both')

    fig.text(0.53, 0.14, x_label, ha='center')
    fig.text(0.004, 0.55, y_label, va='center', rotation='vertical')

# Create the legend
fig.legend(ncol=3,
           handles=sc_handles,      # The line objects
           labels=sc_labels,        # The labels for each line
           loc='lower center',      # Small spacing around legend box
           frameon=False)

fig.tight_layout()
fig.subplots_adjust(bottom=0.2, left=0.11)
plt.savefig(output_path)
plt.show()
